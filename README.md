# README #

Guide on how to get setup with scala & sbt

### Prerequisite 
* sbt

### Usage

##For normal usage

On your shell do the following

```$ sbt```

This should present you with the sbt prompt

After this you can run commands like `run` and `configure` in the sbt prompt to run the scala code

NOTE: `sbt` will download all the required dependencies which might take some time

## To run with emacs

Add the following line to your `plugins.sbt` located at `$HOME/.sbt/<sbt-version>/plugins/plugins.sbt`

example `/home/joel/.sbt/0.13/plugins/plugins.sbt`

Run `sbt gen-ensime` & `sbt gen-ensime-project` in the project root directory like such

```/home/joel/scala-tryouts $ sbt gen-ensime && sbt gen-ensime-project```

Start your emacs.

Switch over to `Main.scala` located in the `src/main/scala` directory in emacs

If you are using my emacs config then press `f9`.
 
This will prompt you to select the ensime file. 

After selecting the `ensime-file` it will start a download for [ensime-server](https://github.com/ensime/ensime-server). Once the download is complete you will have facilities like `autocompletion`, `error highlighting` so on and so forth.

To run SBT through emacs press `f5`

To run scala repl through emacs press `f8`

### References
* http://ensime.github.io/editors/emacs/
* http://ensime.github.io/build_tools/sbt/