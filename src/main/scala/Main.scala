import scala.TDraw
import scala.TSierpinski


object Main {
  def main(args: Array[String]) {
    val spt: TSierpinski = SierpinskiImperative
    val spd: TDraw = DrawAscii
    spd.draw(spt.sierpinski(3))
  }
}
