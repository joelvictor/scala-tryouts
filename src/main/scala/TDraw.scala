package scala

import scala.collection.LinearSeq

trait TDraw {
  def draw (s: LinearSeq[String])
}
