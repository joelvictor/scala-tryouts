package scala

import scala.collection.mutable.LinkedList
import scala._

object SierpinskiImperative extends TSierpinski {
  def sierpinski(n: Int): LinkedList[String]={
    var down = LinkedList("*")
    var space:String = " ";
    for (i <- 0 to n) {
      var nextDown = LinkedList[String]()
      for ( x <- down ){
        nextDown = nextDown:+(space++x++space)
      }
      for ( x <- down ){
        nextDown = nextDown:+(x++" "++x)
      }
      down = nextDown
      space = space ++ space
    }
    return down
  }
}
