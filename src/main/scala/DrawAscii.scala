package scala
import scala.collection.LinearSeq
import scala._

object DrawAscii extends TDraw {
  def draw(x: LinearSeq[String]) {
    x foreach println
  }
}
