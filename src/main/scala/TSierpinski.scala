package scala
import scala.collection.LinearSeq

trait TSierpinski {
  def sierpinski (n:Int): LinearSeq[String]
}
